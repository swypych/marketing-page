(function() {
    'use strict';

    angular
        .module('app')
        .config(['$locationProvider', '$stateProvider', '$urlRouterProvider', '$mdThemingProvider',
            '$mdIconProvider', Config
        ]);

    function Config($locationProvider, $stateProvider, $urlRouterProvider, $mdThemingProvider,
        $mdIconProvider) {
        // enable html5Mode for pushstate ('#'-less
        $locationProvider.html5Mode(true);
        $locationProvider.hashPrefix('!');

        $stateProvider
            .state('home', {
                url: '/app',
                templateUrl: 'views/main.html',
                controller: 'MainController',
                controllerAs: 'mainCtrl',
                cache: false,
                abstract: true
            })
            .state('home.dashboard', {
                url: '/dashboard',
                controller: 'DashboardController',
                templateUrl: 'views/dashboard.html',
                data: {
                    title: 'Dashboard',
                    roles: ['user']
                }
            })
            .state('home.profile', {
                url: '/profile',
                templateUrl: 'views/profile.html',
                controller: 'ProfileController',
                controllerAs: 'profileCtrl',
                data: {
                    title: 'Profile',
                    roles: ['user']
                }
            })
            .state('home.bracelets', {
                url: '/bracelets',
                templateUrl: 'views/bracelets.html',
                controllerAs: 'bCtrl',
                controller: 'BraceletController',
                data: {
                    title: 'Bracelets',
                    roles: ['user']
                }
            })
            .state('home.admin', {
                url: '/admin',
                controller: 'AdminController',
                controllerAs: 'adminCtrl',
                templateUrl: 'views/admin.html',
                data: {
                    title: 'Admin',
                    roles: ['admin']
                }
            })
            .state('home.accessdenied', {
                url: '/accessdenied',
                templateUrl: 'views/partials/accessdenied.html',
                data: {
                    title: 'Access denied',
                    roles: ['']
                }
            })
            .state('home.sigin', {
                url: '/sigin',
                controller: 'LoginController',
                controllerAs: 'loginCtrl',
                templateUrl: 'views/partials/authlogin.html',
                data: {
                    title: 'Sigin',
                    roles: ['']
                }
            })
           .state('home.register', {
                url: '/register',
                controller: 'LoginController',
                controllerAs: 'loginCtrl',
                templateUrl: 'views/partials/authregister.html',
                data: {
                    title: 'Register',
                    roles: ['']
                }
            });;

        $urlRouterProvider.otherwise('/app/dashboard');

        $mdThemingProvider
            .theme('default')
            .primaryPalette('grey', {
                'default': '600'
            })
            .accentPalette('teal', {
                'default': '500'
            })
            .warnPalette('defaultPrimary');

        $mdThemingProvider.theme('dark', 'default')
            .primaryPalette('defaultPrimary')
            .dark();

        $mdThemingProvider.theme('grey', 'default')
            .primaryPalette('grey');

        $mdThemingProvider.theme('custom', 'default')
            .primaryPalette('defaultPrimary', {
                'hue-1': '50'
            });

        $mdThemingProvider.definePalette('defaultPrimary', {
            '50': '#FFFFFF',
            '100': 'rgb(255, 198, 197)',
            '200': '#E75753',
            '300': '#E75753',
            '400': '#E75753',
            '500': '#E75753',
            '600': '#E75753',
            '700': '#E75753',
            '800': '#E75753',
            '900': '#E75753',
            'A100': '#E75753',
            'A200': '#E75753',
            'A400': '#E75753',
            'A700': '#E75753'
        });
        //
        //$mdIconProvider.icon('user', 'assets/images/user.svg', 64);
    }

    angular
        .module('app')
        .run(function($location, $state, $rootScope, authService, $log) {
            var postLoginState;

            $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
                $log.log('$stateChangeStart to ' + toState.name + '- fired when the transition begins. toState,toParams : \n', toState, toParams);

                // if the principal is resolved, do an authorization check immediately. otherwise,
                // it'll be done when the state it resolved.
                authService.checkAuthorization(event, toState, toParams);
            });
            $rootScope.$on('$stateChangeError', function(event, toState, toParams, fromState, fromParams, error) {
                $log.log('$stateChangeError - fired when an error occurs during transition.');
                $log.log(arguments);
            });
            $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
                $log.log('$stateChangeSuccess to ' + toState.name + '- fired once the state transition is complete.');
            });
            // $rootScope.$on('$viewContentLoading',function(event, viewConfig){
            //   // runs on individual scopes, so putting it in "run" doesn't work.
            //   $log.log('$viewContentLoading - view begins loading - dom not rendered',viewConfig);
            // });
            $rootScope.$on('$viewContentLoaded', function(event) {
                $log.log('$viewContentLoaded - fired after dom rendered', event);
            });
            $rootScope.$on('$stateNotFound', function(event, unfoundState, fromState, fromParams) {
                $log.log('$stateNotFound ' + unfoundState.to + '  - fired when a state cannot be found by its name.');
                $log.log(unfoundState, fromState, fromParams);
            });

        });

}());
