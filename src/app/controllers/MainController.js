(function() {
    'use strict';

    angular
    .module('app')
    .controller('MainController',
                ['navService',
                 'deviceService',
                 '$mdSidenav',
                 '$mdBottomSheet',
                 '$log',
                 '$q',
                 '$state',
                 '$mdToast',
                 'userService',
                 'authService',
                 '$scope',
                 MainController
                 ]);
                                                 
    function MainController(navService, deviceService, $mdSidenav, $mdBottomSheet, $log, $q, $state, $mdToast, 
            userService, authService, $scope) {
        var self = this;

        self.scope = $scope;
        self.deviceService = deviceService;
        self.menuItems = [];
        self.selectItem = selectItem;
        self.toggleItemsList = toggleItemsList;
        self.showActions = showActions;
        self.title = $state.current.data.title;
        self.showSimpleToast = showSimpleToast;
        self.userName = userService.user.firstname+' '+ userService.user.lastname;
        self.authenticated = userService.user.uuid !== -1 ? true : false;

        navService
            .loadAllItems()
            .then(function(menuItems) {
                self.menuItems = [].concat(menuItems);
            });

        self.logout = function() {
            authService.logOut();
        };
        
        function toggleItemsList() {
            var pending = $mdBottomSheet.hide() || $q.when(true);

            pending.then(function() {
                $mdSidenav('left').toggle();
            });
        }

        function selectItem(item) {
            self.title = item.name;
            self.toggleItemsList();
            self.showSimpleToast(self.title);
        }

        function showActions($event) {
            $mdBottomSheet.show({
                parent: angular.element(document.getElementById('content')),
                templateUrl: 'views/partials/bottomSheet.html',
                controller: ['$mdBottomSheet', SheetController],
                controllerAs: 'vm',
                bindToController: true,
                targetEvent: $event
            }).then(function(clickedItem) {
                clickedItem && $log.debug(clickedItem.name + ' clicked!');
            });

            function SheetController($mdBottomSheet) {
                var self = this;

                self.actions = [{
                    name: 'Share',
                    icon: 'share',
                    url: 'https://twitter.com/intent/tweet?text=Angular%20Material%20Dashboard%20https://github.com/flatlogic/angular-material-dashboard%20via%20@flatlogicinc'
                }, {
                    name: 'Star',
                    icon: 'star',
                    url: 'https://github.com/flatlogic/angular-material-dashboard/stargazers'
                }];

                self.performAction = function(action) {
                    $mdBottomSheet.hide(action);
                };
            }
        }

        deviceService.getDevices(self)

        self.updateUI = function() {
            if (!self.scope.$$phase) {
                self.scope.$apply();
            }
        }

        function showSimpleToast(title) {
            // $mdToast.show(
            //     $mdToast.simple()
            //     .content(title)
            //     .hideDelay(2000)
            //     .position('top right')
            // );
        }
    }
    
})();
