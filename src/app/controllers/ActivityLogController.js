(function () {
    'use strict';
    
    angular
    .module('app')
    .controller('ActivityLogController', ['$http', 'URLS', 'authService', 'deviceService',
            ActivityLogController
        ]);

    function ActivityLogController($http, URLS, authService, deviceService) {
        var self = this;
        
        this.logs = [];
        
        this.devices = deviceService.getDevices();

        
        // TODO: move data to the service
        self.AcitvityLogData = [{key: 'Mobile', y: 5264}, { key: 'Desktop', y: 3872} ];

        self.chartOptions = {
            chart: {
                type: 'barChart',
                height: 210,
                donut: false,
                x: function (d) { return d.key; },
                y: function (d) { return d.y; },
                valueFormat: (d3.format('.0f')),
                color: ['rgb(0, 150, 136)', '#E75753'],
                showLabels: false,
                showLegend: false,
                title: 'Over 9K',
                margin: { top: -10 }
            }
        };
        
        self.getLog = function() {
            $http({
                url: URLS.BASE_API+ '/api/v1/bracelets/'+authService.buid+'/logs',
                method: 'GET'
            }).then(function successCallback(response) {
                // this callback will be called asynchronously
                // when the response is available
                self.logs = response.data;
            }, function errorCallback(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                self.logs = 'error';
            });
        };
    }
})();
