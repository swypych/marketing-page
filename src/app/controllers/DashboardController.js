(function() {
    'use strict';
    
    angular
    .module('app')
    .controller('DashboardController',
                ['deviceService', '$scope', DashboardController]
                );

    function DashboardController(deviceService, $scope) {
      var self = this;
      self.scope = $scope
      self.deviceService = deviceService

      self.updateUI = function() {
          if (!self.scope.$$phase) {
              self.scope.$apply();
          }
      }

      self.getBraceletInfo = function() {
          //deviceService.
      }

      $scope.config = {
        title: '',
        tooltips: false,
        labels: false,
        mouseover: function() {},
        mouseout: function() {},
        click: function() {},
        legend: {
          display: false,
          //could be 'left, right'
          position: 'right'
        },
        yAxisTickFormat: 's',
        yAxisDomain: [0,100],
        waitForHeightAndWidth: true,
        lineLegend: 'lineEnd'
      };

      $scope.data = {
          series: [''],
          data: [{
            x: "10",
            y: [100],
          }, {
            x: "9",
            y: [80]
          }, {
            x: "6",
            y: [65]
          }, {
            x: "4",
            y: [58]
          }, {
            x: "2",
            y: [50]
          }, {
            x: "0",
            y: [45]
          }]
      };

      $scope.activityConfig = {
        title: '',
        tooltips: false,
        labels: false,
        mouseover: function() {},
        mouseout: function() {},
        click: function() {},
        legend: {
          display: false,
          //could be 'left, right'
          position: 'right'
        },
        colors: [
          'steelBlue'],
        yAxisTickFormat: 's',
        yAxisDomain: [0,70],
        waitForHeightAndWidth: true,
        lineLegend: 'lineEnd'
      };

      $scope.activityData = {
          series: [''],
          data: [{
            x: "Mo",
            y: [32],
          }, {
            x: "Tu",
            y: [51]
          }, {
            x: "Wed",
            y: [65]
          }, {
            x: "Thu",
            y: [58]
          }, {
            x: "Fri",
            y: [50]
          }, {
            x: "Sat",
            y: [42]
          }, {
            x: "Sun",
            y: [45]
          }]
      };

      $scope.bracelet = deviceService.currentBracelet

      // TODO following updates twice
      $scope.$watchCollection('[bracelet.name, bracelet.phone, bracelet.location, bracelet.description]', 
          function (newValues, oldValues) {
            var newData = false
            for (var i=0; i<newValues.length; i++) {
              if (newValues[i] && newValues[i] != oldValues[i]) {
                  newData = true;
                  break;
              }
            }
            if (!newData) {
              return;
            }
            console.log(newValues + "    " + oldValues)
            deviceService.applyBraceletChanges($scope.bracelet);
      });

      $scope.braceletLogs = [];

      deviceService.getBraceletLogs(function(response) {
          $scope.braceletLogs = response;
      });

      $scope.chartActive = true

      $scope.checkinIntervals = ["1 hour", "2 hours", "3 hours", "5  hours", "8 hours", "12 hours", "24 hourz", "2 days", "1 week"];
    }
})();