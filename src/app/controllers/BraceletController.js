(function() {
    'use strict';
    
    angular
    .module('app')
    .controller('BraceletController',
                ['deviceService', '$scope', BraceletController]
                );

    function BraceletController(deviceService, $scope) {
        var self = this;
        self.scope = $scope
        self.deviceService = deviceService
        deviceService.getDevices(self);

        self.updateUI = function() {
            if (!self.scope.$$phase) {
                self.scope.$apply();
            }
        }

        self.setBracelet = function(bracelet) {
            self.deviceService.setBracelet(bracelet)
        }
    }
})();
