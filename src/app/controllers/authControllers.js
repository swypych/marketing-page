(function() {
    'use strict';

    angular
        .module('AuthModule')
        .controller('LoginController', ['$http',
            '$localStorage',
            'authService',
            '$log',
            LoginController
        ]);

    function LoginController($http, $localStorage, authService, $log) {
        var self = this;

        self.user = {
            password: '',
            firstname: '',
            lastname: '',
            email: '',
            phone: '',
            loginFailed: false,
            registrationFailed: false
        };

        self.login = function() {
            $log.log('Logging in ...');
            return authService.logIn(this.user);
        };

        self.register = function() {
            $log.log('Registering in ...');
            return authService.register(this.user);
        };

        self.showRegistrationPage = function() {
            $log.log('Showing registration page');
            return authService.showRegistrationPage(this.user);
        }
    };

}());
