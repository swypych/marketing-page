(function() {
    'use strict';
    var homeCtrl = angular.module('homeController', ['authServices']);

    homeCtrl.controller('homeController', ['$http', 'URLS', '$localStorage', 'authService','$location', function($http, URLS, $localStorage, authService,$location) {
        var self = this;

        self.logout = function($scope) {
      authService.logOut();
        };
    }]);

}());