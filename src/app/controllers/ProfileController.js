(function(){
    'use strict';

  angular
    .module('app')
    .controller('ProfileController', [ 'userService',
      ProfileController
    ]);

  function ProfileController(userService) {
    var self = this;

    self.user = {
      title: 'Admin',
      email: userService.user.email,
      firstName: userService.user.firstname,
      lastName: userService.user.lastname,
      address: '65 Bremner Blvd.' ,
      ext: "Unit 2910",
      city: 'Toronto' ,
      state: 'ON' ,
      postalCode : 'M5J 0A7'
    };
  }

})();
