(function() {
    'use strict';

    angular
        .module('UserModule', ['ngStorage', 'angular-jwt']);

    angular
        .module('UserModule')
        .service('userService', [
            '$localStorage',
            'jwtHelper',
            '$log',
            userService
        ]);

    function userService($localStorage,jwtHelper, $log) {
        var self = this;

        this.user = {
            uuid: localStorage.getItem('uuid') ? localStorage.getItem('uuid') : -1,
            roles: localStorage.getItem('roles') ? localStorage.getItem('roles') : '',
            tokenExpirationDate: localStorage.getItem('tokenExp') ? localStorage.getItem('tokenExp') : -1,
            token: localStorage.getItem('jwtToken') ? localStorage.getItem('jwtToken') : '',
            tokenPayload: localStorage.getItem('jwtTokenPayload') ? localStorage.getItem('jwtTokenPayload') : '',
            username: localStorage.getItem('username') ? localStorage.getItem('username') : '',
            firstname: localStorage.getItem('firstname') ? localStorage.getItem('firstname') : '',
            lastname: localStorage.getItem('lastname') ? localStorage.getItem('lastname') : '',
            email: localStorage.getItem('email') ? localStorage.getItem('email') : ''
        };

        this.clearData = function() {
            localStorage.removeItem('uuid');
            localStorage.removeItem('roles');
            localStorage.removeItem('tokenExp');
            localStorage.removeItem('jwtToken');
            localStorage.removeItem('jwtTokenPayload');
            localStorage.removeItem('username');
            localStorage.removeItem('firstname');
            localStorage.removeItem('lastname');
            localStorage.removeItem('email');

            self.user.uuid = -1;
            self.user.tokenExpirationDate = -1;
            self.user.token = '';
            self.user.tokenPayload = '';
            self.user.roles = '';
            self.user.username = '';
            self.user.firstname = '';
            self.user.lastname = '';
            self.user.email = '';
        };

        this.setToken = function(jwtToken) {
            self.user.token = jwtToken;
            localStorage.setItem('jwtToken', self.user.token);
            self.user.tokenPayload = jwtHelper.decodeToken(jwtToken);
            localStorage.setItem('jwtTokenPayload', self.user.tokenPayload);
            $log.log('tokenpayload\n' + JSON.stringify(self.user.tokenPayload));
            self.user.uuid = self.user.tokenPayload.uuid;
            localStorage.setItem('uuid', self.user.uuid);
            self.user.roles = self.user.tokenPayload.roles;
            localStorage.setItem('roles', self.user.roles);
            self.user.tokenExpirationDate = jwtHelper.getTokenExpirationDate(jwtToken);
            localStorage.setItem('tokenExp', self.user.tokenExpirationDate);
            self.user.firstname = this.user.tokenPayload.firstname;
            localStorage.setItem('firstname', self.user.firstname);
            self.user.lastname = this.user.tokenPayload.lastname;
            localStorage.setItem('lastname', self.user.lastname);
            self.user.email = this.user.tokenPayload.email;
            localStorage.setItem('email', self.user.email);
        };

        this.getToken = function() {
            return localStorage.getItem('jwtToken');
        };

        this.getFirstName = function() {
            return this.user.tokenPayload.firstname;
        };

        this.getLastName = function() {
            return this.user.tokenPayload.lastname;
        };

    }

}());
