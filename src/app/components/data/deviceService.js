(function() {
    'use strict';
    
    angular
    .module('deviceModules', ['ngStorage', 'angular-jwt']);
    
    angular
    .module('deviceModules')
    .service('deviceService', ['$localStorage', 'userService', '$http', 'URLS', '$log', deviceService]);

    function deviceService($localStorage, userService, $http, URLS, $log) {
        var self = this;

        self.devices = [];
        self.observers = []

        var requestInProgress = false;

        self.getDevices = function(observer) {
            if (observer) {
                self.observers.push(observer)
            }
            if (requestInProgress) {
                return;
            }
            requestInProgress = true
            $http({
                url: URLS.BASE_API + '/users/'+userService.user.uuid+'/devices',
                method: 'GET'
            }).then(function successCallback(response) {
                // this callback will be called asynchronously
                // when the response is available
                self.devices = response.data;
                $log.log(response.data);
                for (var i = 0; i < self.observers.length; i++) {
                    self.observers[i].updateUI();
                }
                requestInProgress = false
            }, function errorCallback(response) {
                $log.log('device error' + response);
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                $log.log(response);
                requestInProgress = false
            });
        };

        self.currentBracelet;

        self.setBracelet = function(buid) {
            for (var i=0; i< self.devices.length; i++) {
                if (self.devices[i].buid == buid) {
                    self.currentBracelet = self.devices[i]
                }
            }
        }

        self.applyBraceletChanges = function(bObject) {
            $http({
                url: URLS.BASE_API + '/users/'+userService.user.uuid+'/applyBraceletChanges',
                method: 'PUT',
                data: bObject
            }).then(function successCallback(response) {
                // this callback will be called asynchronously
                // when the response is available
                $log.log("bracelet data updated on server")
            }, function errorCallback(response) {
                $log.log('device error' + response);
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                $log.error(response);
                $log.error("Could not update bracelet data on server");
            });
        }

        self.getBraceletLogs = function(func) {
            $http({
                url: URLS.BASE_API + '/users/'+userService.user.uuid+'/braceletLogs',
                method: 'POST',
                data: {buid: self.currentBracelet["buid"]}
            }).then(function successCallback(response) {
                func(response.data)
            }, function errorCallback(response) {
            });
        }
    }

}());
