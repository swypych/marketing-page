(function() {
    'use strict';

    angular
    .module('app')
    .service('navService', ['authService', '$q', navService]);

    function navService(authService, $q) {
        var menuItems = [{
            name: 'Profile',
            icon: 'face',
            sref: '.profile',
            role: 'user'
        }, {
            name: 'Bracelets',
            icon: 'watch',
            sref: '.bracelets',
            role: 'user'
        }];

        if (authService.isInRole('admin')) {
            menuItems.push({
                name: 'Admin',
                icon: 'contact_mail',
                sref: '.admin',
                role: 'admin'
            });
        }

        return {
            loadAllItems: function() {
                return $q.when(menuItems);
            }
        };
    }

})();
