(function() {
    'use strict';

    angular
        .module('AuthModule', ['ngStorage', 'angular-jwt', 'UserModule']);

    angular
        .module('AuthModule')
        .service('authService', ['$q',
            '$state',
            '$localStorage',
            'jwtHelper',
            '$http',
            'URLS',
            '$log',
            'userService',
            authService
        ]);

    function authService($q, $state, $localStorage, jwtHelper, $http, URLS, $log, userService) {
        var self = this;

        this._desiredState = {
            state: undefined,
            params: undefined
        };

        this.logOut = function() {
            userService.clearData();

            $log.log('logout');
            $state.go('home.sigin', {}, {
                reload: true
            });
        };

        this.logIn = function($user) {
            $http({
                url: URLS.BASE_API + '/users/login',
                skipAuthrization: true,
                method: 'POST',
                data: {
                    email: $user.email,
                    password: $user.password
                }
            }).then(function successCallback(response) {
                // this callback will be called asynchronously
                // when the response is available
                $log.log('Callback login success: ' + response.data);
                userService.setToken(response.data);
                if (self._desiredState.state !== undefined && self._desiredState.state.name !== undefined) {
                    $log.log('login to desired state');
                    $log.log(JSON.stringify(self._desiredState));
                    $state.go(self._desiredState.state.name, self._desiredState.params, {
                        reload: true
                    });
                    delete self._desiredState.state.name;
                    delete self._desiredState.params;
                } else {
                    $log.log('login to default');
                    $state.go('home.dashboard', {}, {
                        reload: true
                    });
                }
                $user.loginFailed = false
            }, function errorCallback(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                $user.loginFailed = true;
                $log.log('Callback login error: ' + response);
            });
        };

        this.register = function($user) {
            $http({
                url: URLS.BASE_API + '/users/register',
                skipAuthrization: true,
                method: 'POST',
                data: {
                    email: $user.email,
                    password: $user.password,
                    firstname: $user.firstname,
                    lastname: $user.lastname,
                    phone: $user.phone
                }
            }).then(function successCallback(response) {
                // this callback will be called asynchronously
                // when the response is available
                self.logIn($user)
                $user.registrationFailed = false
                $log.log('Callback register success: ' + response.data);
            }, function errorCallback(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                $user.registrationFailed = true
                $log.log('Callback register error: ' + response);
            });
        };

        this.showRegistrationPage = function($user) {
            $state.go('home.register', {}, {
                reload: true
            });
        }

        this.isAuthenticated = function() {
            if (userService.user.token === '') {
                return false;
            }
            $log.log('self.user.token' + JSON.stringify(userService.user));
            $log.log('self.user.tokenExpirationDate ' + userService.user.tokenExpirationDate);
            $log.log('jwtHelper.isTokenExpired(self.user.token) ' + jwtHelper.isTokenExpired(userService.user.token));
            $log.log('self.user.roles ' + userService.user.roles);
            $log.log('self.user.uuid ' + userService.user.uuid);
            if (userService.user.uuid !== -1 && !jwtHelper.isTokenExpired(userService.user.token)) {
                $log.log('auth true');
                return true;
            } else {
                $log.log('auth false');
                return false;
            }
        };

        this.setDesiredState = function(state, params) {
            self._desiredState.state = state;
            self._desiredState.params = params;
            $log.log('stored state:' + self._desiredState.state + '\nparams:' + self._desiredState.params);
        };

        this.isInRole = function(role) {
            if (userService.user.uuid === -1 || !userService.user.roles || userService.user.roles === '') {
                return false;
            }

            return userService.user.roles.indexOf(role) !== -1;
        };

        this.isInAnyRole = function(roles) {
            if (userService.user.roles === '' && roles === '') {
                return true;
            }

            for (var i = 0; i < userService.user.roles.length; i++) {
                if (this.isInRole(roles[i])) {
                    return true;
                }
            }

            return false;
        };

        this.checkAuthorization = function(event, state, params) {
            $log.log('authorize ' + JSON.stringify(state.name));
            $log.log('auth ' + this.isAuthenticated());
            $log.log('roles' + userService.user.roles);
            $log.log('roles length' + userService.user.roles.length);
            $log.log('actual state' + state.name);
            $log.log('actual roles' + state.data.roles);
            $log.log('authorized ' + this.isInAnyRole(state.data.roles));

            if (this.isAuthenticated()) {
                if (!this.isInAnyRole(state.data.roles)) {
                    event.preventDefault();
                    $log.log('go to accessdenied state');
                    $state.go('home.accessdenied', {}, {
                        reload: true
                    }); // user is signed in but not authorized for desired state
                }
            } else if (state.name !== 'home.sigin' && state.name !== 'home.register') {
                $log.log('go to sigin state');
                self.setDesiredState(state, params);
                event.preventDefault();
                userService.clearData();
                // now, send them to the signin state so they can log in
                $state.go('home.sigin', {}, {
                    reload: true
                });
            }

        };
    }

    angular
        .module('AuthModule')
        .factory('tokenInterceptor', ['$log', 'userService', 'URLS', function($log, userService, URLS) {
            $log.debug('$log is here to show you that this is a regular factory with injection');

            var tokenInterceptor = {
                request: function(config) {
                    var token = userService.user.token;
                    if (config.url.indexOf(URLS.BASE_API) === 0 && token) {
                        config.headers.Authorization = 'Bearer ' + token;
                    }

                    return config;
                }
            };

            return tokenInterceptor;
        }]);

    angular
        .module('AuthModule')
        .config(['$httpProvider', function($httpProvider) {
            $httpProvider.interceptors.push('tokenInterceptor');
        }]);

}());
