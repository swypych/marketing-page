(function() {
    'use strict';

    angular
        .module('app', ['ngAnimate',
            'angularCharts',
            'ngCookies',
            'ngSanitize',
            'ui.router',
            'ngMaterial',
            'AuthModule',
            'deviceModules',
            'nvd3',
            'AdminModule',
            'gridshore.c3js.chart',
        ]);

    angular
        .module('app')
        .constant('URLS', {
            BASE: 'http://localhost:3000',
            BASE_API: 'http://dev-annaserver.rhcloud.com/api/v1'
        });

    angular.module('app').directive('updateOnEnter', function() {
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function(scope, element, attrs, ctrl) {
                element.on("keyup", function(ev) {
                    if (ev.keyCode == 13) {
                        ctrl.$commitViewValue();
                        scope.$apply(ctrl.$setTouched);
                    }
                });
            }
        }
    });

})();
